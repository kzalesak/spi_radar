# SPI communicator with ADF4158
# v1.3
#
# Changelog:
# 1.2 changed to xfer2, added loop, more comments
# 1.3 added new configuration, added sleep time, set delay time 0, speed 10kHz
# 2.0rc loads from csv, has interface
# 2.0rc2 fixed clock polarity from 1 to 0
#
# COPYRIGHT JAKUB ZALESAK
# Licence GNU-GPL v3.0

import spidev
import time
import sys
import csv

def selectAndSplit(seqDict, seqName):
    """Return a list of 8 bit words to send through SPI.

    Keyword arguments:
    seqDict -- dictionary of sequences (variable length)
    seqName -- name of sequence in dictionary (hex values of variable length)

    Side effects:
    Prints an error message upon some exceptions.
    """
    try:
        seqName = str(seqName) #eliminates retrieving el w. index instead of name
        seqDict[seqName] #checks whether sequence exists
        #extend hex values to 32 bits and convert to str
        formattedSeqList = list(map(lambda x: format(x,'08X'),seqDict[seqName]))
        #check whether a register value does not exceed 32 bits
        for elm in formattedSeqList:
            if len(elm) > 8:
                print('Error: ',elm,' is longer then 32 bits.')
                return 0
        splitSeqList = []
        #separate into tuplets (split 32 value into 8 bit words)
        for elm in formattedSeqList:
            x = 0
            while x < len(elm):
                splitSeqList.append(int(elm[x:x+2],16))
                x = x+2
        return splitSeqList
    except KeyError:
        print('Error: no sequence with that name.')
        return 0

def seqImporter(filename, delim=','):
    """Import a csv file of configurations and return them as a dictionary of lists.

    Keyword arguments:
    filename -- name of csv file to be imported, ".csv" included. (string)
    [delim] = ',' -- delimiter to be used. Optional. (string)

    CSV format:
    Individual configurations as rows in CSV file, in order to be written to chip.
    First row will be used as names.

    Side effects:
    Prints an error message upon FileNotFoundError exception.
    """

    #add fallback for empty fills
    try:
        with open(filename, newline='') as csvfile:
            seqReader = csv.reader(csvfile, delimiter=delim, quotechar='|')
            seqDict = {}
            for row in seqReader:
                seqName = row.pop(0)
                seqDict[seqName] = list(map(lambda x: int(x,16),row))
        return seqDict
    except FileNotFoundError:
        print("File ",filename," does not exist. Did you include path and .csv?")
        return 0

#load sequences
seq = seqImporter('sequences2.csv')

#initialise global variables
readout = []

#testing settings
# auto - writes to device and listens to returns (either itself or slave)
# read - pure readot from external device
test_set = "auto";

#SPI setup
spi = spidev.SpiDev()
spi.open(0,0) #opens device spidev0.0 - other is spidev0.1 -> check pinout

#settings
spi.max_speed_hz = 5000000
spi.mode = 0b01 #[CPOL|CPHA], min: 0b00 = 0, max: 0b11 = 3
spi.bits_per_word = 8

#I used the readout for testing and for transfer. It is prefered to sipmle transfers. Can be monitored by
#splitting the SPI cables to the same PI
if test_set == "auto":
    while True:
        try:
            print('\nAvailabale sequences are:', seq)
            inp_seq = str(input('Choose your sequence: '))
            currentSequence = selectAndSplit(seq,inp_seq)
            if currentSequence == 0: #KeyError did not get passed on
                raise KeyError
            readout = []
            print(currentSequence,'transferring')
            for n in range(0,int(len(currentSequence)/4)):
                readout += spi.xfer2(currentSequence[(n*4):((n+1)*4)], 100000, 0, 8) #changed to use xfer2 to keep the LE low between words
                time.sleep(0.1) #sets time between register values
                #xfer doc: xfer2(list of values[, speed_hz, delay_usec, bits_per_word])
            print(readout, "received\n") #prints out DEC values received for crosscheck -> does not guarantee chip received,
            #as chip doesn't send back unless muxout configured
        except KeyError:
            print('This is not a valid sequence name.')
        except KeyboardInterrupt:
            print('\nTransfer has finished')
            time.sleep(0.5)
            spi.close()
            sys.exit(0)
